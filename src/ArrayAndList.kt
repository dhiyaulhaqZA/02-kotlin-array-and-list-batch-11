fun main(args: Array<String>) {
    val names = arrayOf("Array1", "Array2", "Array3") // ini array
    val nameList = listOf("List1", "List2", "List3") // ini list
    val nameMutableList = mutableListOf("Mutable1", "Mutable2", "Mutable3") // ini mutableList

    names[0] = "Bisa diubah" // array bisa dirubah datanya tp jumlah indexnya fix
    println("Array :")
    // cara menampilkan array satu persatu
    names.forEach {
        println(it)
    }

    println() // baris baru

    println("List :")
    // cara menampilkan list satu persatu
    nameList.forEach {
        println(it)
    }

//    list tidak bisa dirubah datanya maupun jumlah indexnya
//    nameList[0] = "Test" ---> error
//    nameList.add("Test") --> error

//    mutableList bisa dirubah datanya maupun jumlah indexnya
    nameMutableList.add("MutableList4")
    nameMutableList[0] = "Bisa diubah"
    println()
    println("Mutable List :")
    for (name in nameMutableList) {
        println(name)
    }

//    error
//
//    for (Int i = 1; i <= 10; i+2) {
//
//    }

    println()
//    menampilkan angka 1 sampai 10
    for (i in 1..10) {
        println("Hallo : index ke $i")
    }

//    menampilkan angka 1 sampai 10 dengan memberikan selisih 2 angka
    for (i in 1..10 step 2) {
        println("Hallo : index ke $i")
    }

    println()
//    menampilkan angka 10 sampai 1
    for (i in 10 downTo 1) {
        println("Hallo : index ke $i")
    }


// pisang index 0, apel index 1, mangga index 2, jeruk index 3
    val datas = listOf("Pisang", "Apel", "Mangga", "Jeruk")

    // cara menampilkan array dengan index tertentu
    println(datas[3])
    println(datas.get(3))

    println()
//    menampilkan isi datas satu per satu
    datas.forEach {
        println(it) // it mereferensikan data pada index tertentu secara urut
    }

    println()
//    menampilkan isi datas satu per satu
    for (data in datas) {
        println(data)
    }

    println()
//    menampilkan isi datas satu per satu, beserta indexnya
    for ((i, data) in datas.withIndex()) {
        println("index ke $i : $data")
    }

    println()
// menampilkan isi datas satu per satu secara terbalik, beserta indexnya
    for (i in (datas.size-1) downTo 0) {
        println("Index ke $i : ${datas[i]}")
    }

    val warnaMutableList = mutableListOf("Merah", "Kuning", "Hijau")
    var warnaMutableList1 = mutableListOf("Merah", "Kuning", "Hijau")


    println()

//    warnaMutableList = mutableListOf("Ungu") --> error karena (val) mereferensikan beda object
    warnaMutableList1 = mutableListOf("Ungu") // tidak error karena (var) bisa diubah referensi objectnya
    println(warnaMutableList1)


    val warnaList = listOf("Merah", "Kuning", "Hijau")
    println("Jumlah index warnaList : ${warnaList.size}")

    println()

    println(warnaList[0])
    warnaList.forEach {
        println(it)
    }

    warnaMutableList.add("Biru")
//    warnaList.add("Biru") --> error

    warnaMutableList[0] = "Ungu"
//    warnaList[0] = "Ungu" --> error

    val colors = mutableListOf("Red", "Yellow", "Blue")
    colors.add("Green")
    colors.add("Purple")

    // colors.size berisi angka 4
    // loop 0 sampai 3, 0 until colors.size berarti hanya 0..3, angka 4 di exclude
    for (i in 0 until colors.size) {
        println(colors[i])
    }

    println()

    // menghapus data di colors
    colors.clear()
    // menambah data dari warnaList ke colors
    colors.addAll(warnaList)
    colors.forEach {
        println(it)
    }
    println()

    val mutableList = mutableListOf<Int>() // empty list
    val mutableList1: MutableList<String>? = null // nullable mutableList

    print(mutableList)
}










